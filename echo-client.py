###############
# ECHO CLIENT # 
###############

# Modified from Tutorial: https://realpython.com/python-sockets/
# -> Note: Some variable names, comments and code structure are modified 
#    but the code is generally referenced from the link above.

import socket 
import sys

# Configure address
HOST = sys.argv[1]                   # IP ADDRESS OF SERVER
PORT = int(sys.argv[2])              # PORT OF SERVER

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server: 
    # Connect to server
    print("Connecting to server...")
    server.connect((HOST, PORT)) 
    print("Successfully connected to server...")

    # Send message to server 
    data = bytearray(sys.argv[3], encoding="utf8")
    print(f"==> Sent: {data}")
    server.sendall(data); 

    # Receive message from serve
    data = server.recv(1024) 
    print(f"<== Received: {data}")
