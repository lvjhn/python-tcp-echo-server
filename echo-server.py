###############
# ECHO SERVER # 
###############

# Modified from Tutorial: https://realpython.com/python-sockets/
# -> Note: Some variable names, comments and code structure are modified 
#    but the code is generally referenced from the link above.

import socket
import sys 

# Configure address
HOST = sys.argv[1]                   # IP ADDRESS OF SERVER
PORT = int(sys.argv[2])              # PORT OF SERVER

# Client no
__client_no = 0

# Create a new server instance: 
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
    
    # Bind host and port to server instance 
    print(f"Binding HOST({HOST}) and PORT({PORT}) to server.")
    
    server.bind((HOST, PORT)) 

    # Make server listen 
    print(f"Server is listening at {HOST}:{PORT}")
    server.listen() 

    # Accept incoming connections
    print(f"Server is now accepting connections.")
    while True:
        conn, addr = server.accept() 
        __client_no += 1
        with conn: 
            print(f"Connected by {addr}. Client no: {__client_no}")
            while True: 
                data = conn.recv(1024) 
                if not data: 
                    break 
                print(f" <=== Received: {data}")
                conn.sendall(b"server@echo: " + data) 
                print(f" ===> Sent: {data}")

        


