# Python TCP Echo Server 
A simple/lightweight TCP Echo Server implemented in Python based on RealPython's tutorial (with minor modifications).

## Usage 
### Server:
```
python3 -m echo-server 127.0.0.1 8080 
```
Output (Server):
```
Binding HOST(127.0.0.1) and PORT(8080) to server.
Server is listening at 127.0.0.1:8080
Server is now accepting connections.
```

### Client: 
```
python3 -m echo-client 127.0.0.1 8080 "Hello, World!" 
``` 
Output: 
```
Server: 
    ...
    Connected by ('127.0.0.1', 39834). Client no: 1
    <=== Received: b'Hello, World!'
    ===> Sent: b'Hello, World!'
    <=== Received: b''
Client:
    Connecting to server...
    Successfully connected to server...
    ==> Sent: bytearray(b'Hello, World!')
    <== Received: b'server@echo: Hello, World!'
```

